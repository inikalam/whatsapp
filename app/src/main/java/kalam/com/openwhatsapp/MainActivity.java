package kalam.com.openwhatsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kalam.com.openwhatsapp.R;

public class MainActivity extends AppCompatActivity {

    EditText etNomor;
    Button btnOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNomor = findViewById(R.id.etNomor);
        btnOpen = findViewById(R.id.btnOpen);

        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNomor.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Harap masukkan nomor Handphone yang benar",
                            Toast.LENGTH_SHORT).show();
                } else {
                    String nomorHp = etNomor.getText().toString().substring(1);
                    openWhatsApp("+62" + nomorHp);
                }
            }
        });
    }

    private void openWhatsApp(String nomor) {
        String url = "https://api.whatsapp.com/send?phone=" + nomor;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
        etNomor.setText("");
    }
}
